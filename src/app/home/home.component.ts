import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  currentState: string = "questions";

  surveyForm = new FormGroup({

    questions: new FormGroup({
      reponse1: new FormControl(),
      reponse2: new FormControl()
    }),
    coordonees: new FormGroup({
      email: new FormControl(),
      nom: new FormControl(),
    }),

  })

  constructor() { }

  ngOnInit(): void {
  }

  get reponse1() {
    return this.surveyForm.get("questions")?.get("reponse1")?.value;
  }
  get reponse1array() {
    let userThemes: string = this.surveyForm.get("questions")?.get("reponse1")?.value;
    let themes: string[] = userThemes.split(',').map(theme => theme.trim());
    return themes;
  }

  get reponse2() {
    return this.surveyForm.get("questions")?.get("reponse2")?.value;
  }
  get nom() {
    return this.surveyForm.get("coordonnees")?.get("nom")?.value;
  }
  get email() {
    return this.surveyForm.get("coordonnees")?.get("email")?.value;
  }

  onSubmit() {
    switch (this.currentState) {
      // if this.currentState == "questions"
      case "questions":
        this.currentState = "coordonnees";
        console.log("Réponse 2 :" + this.reponse2);
        break;
      // if this.currentState == "coordonnees"
      case "coordonnees":
        this.currentState = "recap";
        break;
    }
  }

}
